package com.example.raffi.popularmovies;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CheckableImageButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MoviePosterAdapter extends RecyclerView.Adapter {

    private List<MoviePoster> moviePosters = new ArrayList<>();
	private List<MoviePoster> favoriteMovies = new ArrayList<>();
	private Comparator<MoviePoster> moviePosterComparator = new MoviePosterComparators.ByPopularity();
    private final Context context;

    MoviePosterAdapter(Context context, List<MoviePoster> moviePosters) {
        super();
        this.context = context;
        this.moviePosters = moviePosters;
    }

    public void setMoviePosters(List<MoviePoster> moviePosters) {
    	if (moviePosters == null) {
    		this.moviePosters = new ArrayList<>();
	    } else {
		    this.moviePosters = moviePosters;
	    }
    	notifyDataSetChanged();
    }

	public void setFavoriteMovies(List<MoviePoster> favoriteMovies) {
		this.favoriteMovies = favoriteMovies;
		favoriteMovies.sort(moviePosterComparator);
	}

	public void showFavoriteMovies() {
    	setMoviePosters(favoriteMovies);
	}

	public void setMoviePosterComparator(Comparator<MoviePoster> comparator) {
    	if (moviePosterComparator.getClass() != comparator.getClass()) {
		    moviePosterComparator = comparator;
		    favoriteMovies.sort(moviePosterComparator);
	    }
	}

    @NonNull
    @Override
    public MoviePosterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		ConstraintLayout rootViewOfXML = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_movie_poster, parent, false);

		//get supplies for calculating and setting layout params
		Resources resources = context.getResources();
		int largeSpacing = resources.getDimensionPixelSize(R.dimen.spacing_large);

		GridLayoutManager.LayoutParams rootViewLayoutParams = (GridLayoutManager.LayoutParams) rootViewOfXML.getLayoutParams();

		// set layout params for root view
		int posterViewWidth = parent.getWidth() / 2;
		int imageViewWidth = Math.round(posterViewWidth - largeSpacing * 1.5f);
		int imageViewHeight = Math.round(imageViewWidth * 3f / 2f);
		int posterViewHeight = imageViewHeight + largeSpacing;

		rootViewLayoutParams.height = posterViewHeight;

		rootViewOfXML.setLayoutParams(rootViewLayoutParams);

		// margin for image container (cardview)
		// if view is in left column
		int leftPadding = largeSpacing;
		int rightPadding = largeSpacing / 2;

		// switch if view is in right column
		if (viewType == 1) {
			int tmp = leftPadding;
			leftPadding = rightPadding;
			rightPadding = tmp;
		}

		int topPadding = largeSpacing / 2;
		int bottomPadding = topPadding;

		CardView cv = rootViewOfXML.findViewById(R.id.cv_movie_poster);
		ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) cv.getLayoutParams();
		marginLayoutParams.setMargins(leftPadding, topPadding, rightPadding, bottomPadding);
		cv.requestLayout();

        return new MoviePosterViewHolder(rootViewOfXML);
    }

	@Override
	public int getItemViewType(int position) {
		return position % 2;
	}

	@Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MoviePoster moviePoster = moviePosters.get(position);

        final MoviePosterViewHolder moviePosterViewHolder = (MoviePosterViewHolder) holder;
        final ImageView imageView = moviePosterViewHolder.moviePosterImageView;

        final ToggleButton favoriteButton = moviePosterViewHolder.favoriteButton;
        final boolean isFavorite = favoriteMovies.contains(moviePoster);
        favoriteButton.setChecked(isFavorite);

        favoriteButton.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View view) {
	        	ToggleButton tb = (ToggleButton) view;
	        	FavoriteMovieDao dao = FavoriteMovieDatabase
				        .getDatabase(context)
				        .getFavoriteMovieDao();
		        if (tb.isChecked()) {
		        	// movie poster was already a favorite, so delete from favorites
			        dao.delete(moviePoster);
		        } else {
		        	// movie poster was not initially a favorite, so add to favorites
			        dao.insert(moviePoster);
		        }
	        }
        });

        Picasso.get()
                .load(moviePoster.imageURI)
                .into(imageView);

        View rootLayout = holder.itemView.findViewById(R.id.frame_layout_poster);
        rootLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent startDetail = new Intent(context, DetailActivity.class);
				startDetail.putExtra(context.getString(R.string.extra_movie_poster), moviePoster);
				context.startActivity(startDetail);
			}
		});
    }

    @Override
    public int getItemCount() {
    	if (moviePosters == null) {
    		return 0;
	    }

        return moviePosters.size();
    }

    private class MoviePosterViewHolder extends RecyclerView.ViewHolder {

        final ImageView moviePosterImageView;
        final ToggleButton favoriteButton;

        MoviePosterViewHolder(View view) {
            super(view);

            moviePosterImageView = view.findViewById(R.id.iv_movie_poster);
            favoriteButton = view.findViewById(R.id.btn_favorite);
        }
    }
}
