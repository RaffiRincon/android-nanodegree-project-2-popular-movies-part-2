package com.example.raffi.popularmovies;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ReviewsRecyclerViewAdapter extends RecyclerView.Adapter {

	List<Review> reviews = new ArrayList<>();

	void setReviews(List<Review> reviews) {
		this.reviews = reviews;
		notifyDataSetChanged();
	}


	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_review, parent, false);

		return new ReviewViewHolder(row);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		ReviewViewHolder reviewViewHolder = (ReviewViewHolder) holder;
		Review review = reviews.get(position);

		reviewViewHolder.getReviewTV().setText(review.review);
		reviewViewHolder.getUsernameTV().setText(holder.itemView.getContext().getString(R.string.review_username_format, review.username));
	}

	@Override
	public int getItemCount() {
		if (reviews == null) {
			return 0;
		}
		return reviews.size();
	}

	class ReviewViewHolder extends RecyclerView.ViewHolder {
		TextView reviewTV;
		TextView usernameTV;

		public ReviewViewHolder(View itemView) {
			super(itemView);
			reviewTV = itemView.findViewById(R.id.tv_review);
			usernameTV = itemView.findViewById(R.id.tv_review_username);
		}

		public TextView getReviewTV() {
			return reviewTV;
		}

		public TextView getUsernameTV() {
			return usernameTV;
		}
	}
}
