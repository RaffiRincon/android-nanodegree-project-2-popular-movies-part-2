package com.example.raffi.popularmovies;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "favorite_movie")
class MoviePoster implements Parcelable {

	Uri imageURI;
    Uri highResImageUri;
    String title;
    @PrimaryKey
    int id;
    String plotSynopsis;
    Double averageRating;
    Double popularity;
    String releaseDateString;

    MoviePoster() {
    	super();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedObject(imageURI, 0);
		dest.writeTypedObject(highResImageUri, 0);
		dest.writeString(title);
		dest.writeInt(id);
		dest.writeString(plotSynopsis);
		dest.writeDouble(averageRating);
		dest.writeDouble(popularity);
		dest.writeString(releaseDateString);
	}

	private MoviePoster(Parcel in) {
		imageURI = in.readTypedObject(Uri.CREATOR);
		highResImageUri = in.readTypedObject(Uri.CREATOR);
		title = in.readString();
		id = in.readInt();
		plotSynopsis = in.readString();
		averageRating = in.readDouble();
		popularity = in.readDouble();
		releaseDateString = in.readString();
	}

	public static final Parcelable.Creator<MoviePoster> CREATOR =
			new Parcelable.Creator<MoviePoster>() {
		public MoviePoster createFromParcel(Parcel in) {
			return new MoviePoster(in);
		}

		public MoviePoster[] newArray(int size) {
			return new MoviePoster[size];
		}
	};

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof MoviePoster)) {
			return false;
		}

		return ((MoviePoster) obj).id == this.id;
	}

	// Getters and Setters for Room
	public Uri getImageURI() {
		return imageURI;
	}

	public void setImageURI(Uri imageURI) {
		this.imageURI = imageURI;
	}

	public Uri getHighResImageUri() {
		return highResImageUri;
	}

	public void setHighResImageUri(Uri highResImageUri) {
		this.highResImageUri = highResImageUri;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlotSynopsis() {
		return plotSynopsis;
	}

	public void setPlotSynopsis(String plotSynopsis) {
		this.plotSynopsis = plotSynopsis;
	}

	public Double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	public Double getPopularity() {
		return popularity;
	}

	public void setPopularity(Double popularity) {
		this.popularity = popularity;
	}

	public String getReleaseDateString() {
		return releaseDateString;
	}

	public void setReleaseDateString(String releaseDateString) {
		this.releaseDateString = releaseDateString;
	}
}

