package com.example.raffi.popularmovies;

public class Review {

	String username;
	String review;

	public Review(String username, String review) {
		this.username = username;
		this.review = review;
	}

}
