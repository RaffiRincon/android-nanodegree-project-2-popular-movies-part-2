package com.example.raffi.popularmovies;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	private MoviePosterAdapter moviePosterAdapter;
	private MovieSortOrder movieSortOrder = MovieSortOrder.BY_POPULARITY;
	private MoviesToShow moviesToShow = MoviesToShow.ALL;
	private static final String MOVIE_SORT_ORDER_KEY = "sort_order";
	private static final String MOVIES_TO_SHOW_KEY = "movies_to_show";
	private Toast currentToast = null;
	private boolean menuNeedsUpdate = false;
	private MainViewModel viewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		configureRecyclerView();
		restoreFrom(savedInstanceState);
		setTitle(R.string.activity_main_title);

		if (moviesToShow == MoviesToShow.ALL) {
			new MovieDBTask().execute(movieSortOrder);
		}

		// handles the case where moviesToShow is FAVORITES
		configureViewModel();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(MOVIES_TO_SHOW_KEY, moviesToShow.toString());
		outState.putString(MOVIE_SORT_ORDER_KEY, movieSortOrder.toString());

		super.onSaveInstanceState(outState);
	}

	enum MoviesToShow {
		FAVORITES, ALL;
	}

	enum MovieSortOrder {
		BY_POPULARITY, BY_TOP_RATED
	}

	class MovieDBTask extends AsyncTask<MovieSortOrder, Void, List<MoviePoster>> {

		@Override
		protected List<MoviePoster> doInBackground(MovieSortOrder... sortOrders) {
			String jsonString;

			MovieSortOrder sortOrder = sortOrders[0];

			try {
				URL url = null;

				switch (sortOrder) {
					case BY_TOP_RATED:
						url = NetworkUtil.buildTopRatedMoviesURL();
						break;
					case BY_POPULARITY:
						url = NetworkUtil.buildPopularMoviesURL();
				}

				jsonString = NetworkUtil.getResponse(url);

				return JSONUtil.moviePosters(jsonString, MainActivity.this);
			} catch (IOException ioException) {

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (currentToast != null) {
							currentToast.cancel();
						}

						currentToast = Toast.makeText(
								MainActivity.this,
								R.string.no_movies_message,
								Toast.LENGTH_SHORT);

						currentToast.show();
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(List<MoviePoster> moviePosters) {
			if (moviePosters != null && moviePosters.size() > 0) {
				moviePosterAdapter.setMoviePosters(moviePosters);
			}
		}
	}

	private void restoreFrom(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			boolean menuNeedsUpdating = false;

			String savedMovieSortOrderString = savedInstanceState.getString(MOVIE_SORT_ORDER_KEY);
			if (savedMovieSortOrderString.equals(MovieSortOrder.BY_TOP_RATED.toString())) {
				movieSortOrder = MovieSortOrder.BY_TOP_RATED;
				new MovieDBTask().execute(movieSortOrder);
				menuNeedsUpdating = true;
			}

			String savedMoviesToShowString = savedInstanceState.getString(MOVIES_TO_SHOW_KEY);
			if (savedMoviesToShowString.equals(MoviesToShow.FAVORITES.toString())) {
				moviesToShow = MoviesToShow.FAVORITES;
				showFavoritedMoviePosters();
				menuNeedsUpdating = true;
			}

			if (menuNeedsUpdating) {
				updateMenu();
			}
		}
	}

	private void restoreFromViewModel() {
		if (viewModel == null) {
			viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
		}

		this.moviesToShow = viewModel.moviesToShow;
		this.movieSortOrder = viewModel.movieSortOrder;
	}

	private void configureRecyclerView() {
		RecyclerView moviePosterRecyclerView = findViewById(R.id.rv_grid);
		moviePosterAdapter = new MoviePosterAdapter(this, null);
		moviePosterRecyclerView.setAdapter(moviePosterAdapter);

		GridLayoutManager layoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
		moviePosterRecyclerView.setLayoutManager(layoutManager);
	}

	private void configureViewModel() {
		viewModel =ViewModelProviders.of(this).get(MainViewModel.class);
		viewModel.getFavoriteMovies().observe(this, new Observer<List<MoviePoster>>() {
			@Override
			public void onChanged(@Nullable List<MoviePoster> favoriteMovies) {
				moviePosterAdapter.setFavoriteMovies(favoriteMovies);
				if (moviesToShow == MoviesToShow.FAVORITES){
					showFavoritedMoviePosters();
				}
			}
		});
	}

	private void showFavoritedMoviePosters() {
		moviePosterAdapter.showFavoriteMovies();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.isChecked()) {
			return true;
		}

		item.setChecked(true);

		MovieSortOrder newMovieSortOrder = this.movieSortOrder;
		MoviesToShow newMoviesToShow = this.moviesToShow;

		switch (item.getItemId()) {
			case R.id.main_activity_menu_item_popularity:
				newMovieSortOrder = MovieSortOrder.BY_POPULARITY;
				break;
			case R.id.main_activity_menu_item_top_rated:
				newMovieSortOrder = MovieSortOrder.BY_TOP_RATED;
				break;
			case R.id.main_activity_menu_item_show_favorites:
				newMoviesToShow = MoviesToShow.FAVORITES;
				break;
			case R.id.main_activity_menu_item_show_all_movies:
				newMoviesToShow = MoviesToShow.ALL;
				break;
			default:
				return super.onOptionsItemSelected(item);
		}

		if (moviesToShow != newMoviesToShow) {
			moviesToShow = newMoviesToShow;
		}

		if (movieSortOrder != newMovieSortOrder) {
			movieSortOrder = newMovieSortOrder;
			switch (movieSortOrder) {
				case BY_TOP_RATED:
					moviePosterAdapter.setMoviePosterComparator(new MoviePosterComparators.ByTopRated());
					break;
				case BY_POPULARITY:
					moviePosterAdapter.setMoviePosterComparator(new MoviePosterComparators.ByPopularity());
					break;
			}
		}

		switch (moviesToShow) {
			case ALL:
				new MovieDBTask().execute(movieSortOrder);
				return true;
			case FAVORITES:
				showFavoritedMoviePosters();
				return true;
		}

		viewModel.movieSortOrder = movieSortOrder;
		viewModel.moviesToShow = moviesToShow;

		return true;
	}

	private void updateMenu() {
		menuNeedsUpdate = true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.main_activity_menu, menu);

		if (!menuNeedsUpdate) {
			return true;
		} else {
			menuNeedsUpdate = false;
		}

		MenuItem itemForChecking = null;
		switch (movieSortOrder) {
			case BY_POPULARITY:
				itemForChecking = menu.findItem(R.id.main_activity_menu_item_popularity);
				break;
			case BY_TOP_RATED:
				itemForChecking = menu.findItem(R.id.main_activity_menu_item_top_rated);
				break;
		}
		itemForChecking.setChecked(true);

		switch (moviesToShow) {
			case ALL:
				itemForChecking = menu.findItem(R.id.main_activity_menu_item_show_all_movies);
				break;
			case FAVORITES:
				itemForChecking = menu.findItem(R.id.main_activity_menu_item_show_favorites);
				break;
		}
		itemForChecking.setChecked(true);

		return true;
	}
}
