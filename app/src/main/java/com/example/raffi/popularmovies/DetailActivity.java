package com.example.raffi.popularmovies;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailActivity extends AppCompatActivity {

	private MoviePoster moviePoster;
	TrailerRecyclerViewAdapter trailerRecyclerViewAdapter = null;
	ReviewsRecyclerViewAdapter reviewsRecyclerViewAdapter = null;

	FavoriteMovieDao favoriteMovieDao;
	public boolean isDisplayingFavoriteMovie = false;

	private Toast currentToast = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		Intent startThis = getIntent();
		MoviePoster moviePoster = (startThis.getParcelableExtra(getString(R.string.extra_movie_poster)));

		this.moviePoster = moviePoster;
		configureMovieDatabase(); // should be called after this.moviePoster is set

		setTitle(R.string.activity_detail_title);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		configureUI(moviePoster);
	}

	class FetchTrailersTask extends AsyncTask<Integer, Void, List<Trailer>> {

		@Override
		protected List<Trailer> doInBackground(Integer... integers) {
			List<Trailer> trailers = null;

			try {
				int movieId = integers[0];
				String stringResponse = NetworkUtil.getResponse(NetworkUtil.buildTrailersURL(movieId));

			trailers = JSONUtil.trailersFromJSON(stringResponse);
		} catch (IOException e) {
			e.printStackTrace();
		}

			return trailers;
		}

		@Override
		protected void onPostExecute(final List<Trailer> trailers) {
			trailerRecyclerViewAdapter.setTrailers(trailers);
		}
	}

	private void configureMovieDatabase() {
		favoriteMovieDao = FavoriteMovieDatabase.getDatabase(this).getFavoriteMovieDao();
		favoriteMovieDao.getAll().observe( this, new Observer<List<MoviePoster>>() {
			@Override
			public void onChanged(@Nullable List<MoviePoster> moviePosters) {
				boolean newFavoriteMovieStatus = false;

				if (moviePosters != null && moviePosters.contains(moviePoster)) {
					newFavoriteMovieStatus = true;
				}

				if (isDisplayingFavoriteMovie != newFavoriteMovieStatus) {
					isDisplayingFavoriteMovie = newFavoriteMovieStatus;
					invalidateOptionsMenu();
				}
			}
		});
	}

	private void configureUI(MoviePoster moviePoster) {
		ImageView posterImageIV = findViewById(R.id.iv_poster_image_detail);
		Picasso.get().load(moviePoster.highResImageUri).into(posterImageIV);

		TextView popularityTV = findViewById(R.id.tv_popularity_score);
		Locale locale = getResources().getConfiguration().getLocales().get(0);
		popularityTV.setText(String.format(locale, "%.2f", moviePoster.popularity));

		TextView synopsisTV = findViewById(R.id.tv_synopsis);
		synopsisTV.setText(moviePoster.plotSynopsis);

		ImageView thumbsUpView = findViewById(R.id.iv_thumbs_up);
		thumbsUpView.setImageDrawable(getDrawable(R.drawable.ic_thumb_up_24dp));

		TextView releaseDateTV = findViewById(R.id.tv_release_date);
		releaseDateTV.setText(getString(R.string.release_date_label, moviePoster.releaseDateString));

		configureStars(moviePoster.averageRating);

		configureTrailersRecyclerView(moviePoster);

		configureReviewsRecylerView(moviePoster);
	}

	private void configureTrailersRecyclerView(MoviePoster moviePoster) {
		RecyclerView trailersRV = findViewById(R.id.rv_trailers);
		trailerRecyclerViewAdapter = new TrailerRecyclerViewAdapter();

		LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		trailersRV.setLayoutManager(layoutManager);
		trailersRV.setAdapter(trailerRecyclerViewAdapter);
		new FetchTrailersTask().execute(moviePoster.id);
	}

	private void configureReviewsRecylerView(MoviePoster moviePoster) {
		RecyclerView reviewRV = (RecyclerView) findViewById(R.id.rv_reviews);
		reviewsRecyclerViewAdapter = new ReviewsRecyclerViewAdapter();

		LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		reviewRV.setLayoutManager(layoutManager);
		reviewRV.setAdapter(reviewsRecyclerViewAdapter);
		new FetchMovieReviews().execute(moviePoster.id);
	}

	class FetchMovieReviews extends AsyncTask<Integer, Void, List<Review>> {

		boolean shouldToast = false;

		@Override
		protected List<Review> doInBackground(Integer... integers) {
			int movieId = integers[0];

			List<Review> reviews = new ArrayList<>();
			try {
				URL reviewsURL = NetworkUtil.reviewsURL(movieId);
				String response = NetworkUtil.getResponse(reviewsURL);

				reviews = JSONUtil.reviewsFromJSONString(response);

			} catch (IOException e) {
				e.printStackTrace();
			}

			return reviews;
		}

		@Override
		protected void onPostExecute(List<Review> reviews) {
			if (reviews != null) {
				reviewsRecyclerViewAdapter.setReviews(reviews);
			}

			if (shouldToast) {
				if (currentToast != null) {
					currentToast.cancel();
				}
				currentToast = Toast.makeText(DetailActivity.this, getString(R.string.no_reviews_message), Toast.LENGTH_SHORT);
				currentToast.show();
			}
		}
	}

	private void configureStars(Double averageRating) {
		int numberOfStarsLeft = 5;
		averageRating /= 2; // make rating out of 5 instead of out of 10

		ViewGroup layout = findViewById(R.id.average_score_layout);
		int leftViewId = R.id.guideline_average_score;
		while (averageRating > 1) {
			leftViewId = starAfterAddingToLeftOf(leftViewId, layout,
					getDrawable(R.drawable.rating_star_full_24dp));

			numberOfStarsLeft -= 1;
			averageRating -= 1.0;
		}

		if (averageRating >= 0.75) {
			leftViewId = starAfterAddingToLeftOf(leftViewId, layout,
					getDrawable(R.drawable.rating_star_full_24dp));

			numberOfStarsLeft -= 1;
		} else if (averageRating > 0.25) {
			leftViewId = starAfterAddingToLeftOf(leftViewId, layout,
					getDrawable(R.drawable.rating_star_half_24dp));

			numberOfStarsLeft -= 1;
		}

		while (numberOfStarsLeft > 0) {
			leftViewId = starAfterAddingToLeftOf(leftViewId, layout,
					getDrawable(R.drawable.rating_star_empty_24dp));

			numberOfStarsLeft -= 1;
		}

		layout.requestLayout();
	}

	private int starAfterAddingToLeftOf(int id, ViewGroup layout, Drawable starDrawable) {
		ImageView starView = new ImageView(this);
		starView.setImageDrawable(starDrawable);

		ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(starDrawable.getMinimumWidth(), starDrawable.getMinimumHeight());
		layoutParams.leftToLeft = id;
		layoutParams.topToTop = id;
		layoutParams.bottomToBottom = id;

		layout.addView(starView, layout.getChildCount(), layoutParams);

		return starView.getId();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.detail_activity_menu_item_favorite:
				if (isDisplayingFavoriteMovie) {
					// unfavorite movie
					AppExecutors.sharedInstance().getDiskIOExecutor().execute(new Runnable() {
						@Override
						public void run() {
							favoriteMovieDao.delete(moviePoster);
						}
					});
				} else {
					AppExecutors.sharedInstance().getDiskIOExecutor().execute(new Runnable() {
						@Override
						public void run() {
							favoriteMovieDao.insert(moviePoster);
						}
					});
				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.detail_activity_menu, menu);

		MenuItem favoriteMenuItem = menu.findItem(R.id.detail_activity_menu_item_favorite);
		favoriteMenuItem.setTitle(isDisplayingFavoriteMovie ?
				R.string.detail_activity_menu_unfavorite_title :
				R.string.detail_activity_menu_favorite_title);

		return true;
	}
}
