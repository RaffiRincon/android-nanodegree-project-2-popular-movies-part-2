package com.example.raffi.popularmovies;

public class Trailer {

	String id;
	String title;

	public Trailer(String id, String title) {
		this.id = id;
		this.title = title;
	}
}
