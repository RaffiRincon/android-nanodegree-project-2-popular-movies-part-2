package com.example.raffi.popularmovies;

import java.util.Comparator;

public class MoviePosterComparators {


	public static class ByPopularity implements Comparator<MoviePoster> {
		@Override
		public int compare(MoviePoster moviePoster1, MoviePoster moviePoster2) {
			return Double.compare(moviePoster1.popularity, moviePoster2.popularity);
		}
	}

	public static class ByTopRated implements Comparator<MoviePoster> {

		@Override
		public int compare(MoviePoster moviePoster1, MoviePoster moviePoster2) {
			return Double.compare(moviePoster1.averageRating, moviePoster2.averageRating);
		}
	}

}
