package com.example.raffi.popularmovies;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtil {

	public static final String MOVIE_DB_API_KEY = BuildConfig.ApiKey;

	public static final String MOVIE_DB_API_PARAMETER_KEY = "api_key";

	private static final String BASE_MOVIES_PATH = "http://api.themoviedb.org/3/movie/";

	private static final String MOVIES_BY_POPULARITY_PATH = BASE_MOVIES_PATH + "popular";
	private static final String MOVIES_BY_RATING_PATH = BASE_MOVIES_PATH + "top_rated";

	private static final String TRAILERS_PATH = "videos";

	private static final String YOUTUBE_VIDEO_BASE_URL = "https://www.youtube.com/watch";
	private static final String YOUTUBE_VIDEO_PARAMETER_KEY = "v";

	private static final String REVIEWS_PATH = "reviews";

	public static URL buildPopularMoviesURL() {
		Uri.Builder uriBuilder = uriBuilderWithAPIKeySet().encodedPath(MOVIES_BY_POPULARITY_PATH);

		return toURL(uriBuilder.build());
	}

	public static URL buildTopRatedMoviesURL() {
		Uri.Builder uriBuilder = uriBuilderWithAPIKeySet()
				.encodedPath(MOVIES_BY_RATING_PATH);

		return toURL(uriBuilder.build());

	}

	static URL buildTrailersURL(int movieId) {
		Uri.Builder uriBuilder = uriBuilderWithAPIKeySet()
				.encodedPath(BASE_MOVIES_PATH)
				.appendEncodedPath(movieId + "")
				.appendEncodedPath(TRAILERS_PATH);

		return toURL(uriBuilder.build());
	}

	private static Uri.Builder uriBuilderWithAPIKeySet() {
		return new Uri.Builder().appendQueryParameter(MOVIE_DB_API_PARAMETER_KEY, MOVIE_DB_API_KEY);
	}

	private static URL toURL(Uri uri) {
		URL url = null;
		try {
			url = new URL(uri.toString());

		} catch (MalformedURLException badURLException) {
			badURLException.printStackTrace();
		}

		return url;
	}

	public static String getResponse(URL url) throws IOException {
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		try {
			InputStream in = urlConnection.getInputStream();

			Scanner scanner = new Scanner(in);
			scanner.useDelimiter("\\A");

			boolean hasInput = scanner.hasNext();
			if (hasInput) {
				return scanner.next();
			} else {
				return null;
			}
		} finally {
			urlConnection.disconnect();
		}
	}

	static Uri youtubeUriFromYouTubeKey(String key) {
		Uri youtubeUri = new Uri.Builder()
				.encodedPath(YOUTUBE_VIDEO_BASE_URL)
				.appendQueryParameter(YOUTUBE_VIDEO_PARAMETER_KEY, key)
				.build();

		return youtubeUri;
	}

	static URL reviewsURL(int movieID) {
		Uri uri = uriBuilderWithAPIKeySet().
				encodedPath(BASE_MOVIES_PATH)
				.appendPath(movieID + "")
				.appendPath(REVIEWS_PATH)
				.build();

		return toURL(uri);
	}
}