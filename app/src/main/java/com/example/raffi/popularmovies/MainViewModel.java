package com.example.raffi.popularmovies;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

	private LiveData<List<MoviePoster>> favoriteMovies;

	MainActivity.MoviesToShow moviesToShow;
	MainActivity.MovieSortOrder movieSortOrder;

	public MainViewModel(@NonNull Application application) {
		super(application);

		final FavoriteMovieDatabase database = FavoriteMovieDatabase
				.getDatabase(this.getApplication());
		favoriteMovies = database.getFavoriteMovieDao().getAll();
	}

	public LiveData<List<MoviePoster>> getFavoriteMovies() {
		return favoriteMovies;
	}
}
