package com.example.raffi.popularmovies;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TrailerRecyclerViewAdapter extends RecyclerView.Adapter<TrailerRecyclerViewAdapter.TrailerViewHolder> {
	private List<Trailer> trailers = new ArrayList<>();

	public void setTrailers(List<Trailer> trailers) {
		if (trailers == null) {
			this.trailers = new ArrayList<>();
		}

		this.trailers = trailers;

		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public TrailerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View rootViewOfXML = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_trailer_row, parent, false);
		return new TrailerViewHolder(rootViewOfXML);
	}

	@Override
	public void onBindViewHolder(@NonNull TrailerViewHolder holder, int position) {

		final int row = holder.getAdapterPosition();
		final Trailer trailer = trailers.get(row);
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Uri youtubeUri = Uri.parse("vnd.youtube:" + trailer.id);
				try {
					Intent youtubeIntent = new Intent(Intent.ACTION_VIEW, youtubeUri);
					view.getContext().startActivity(youtubeIntent);
				} catch (ActivityNotFoundException e) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW,
							NetworkUtil.youtubeUriFromYouTubeKey(trailer.id));
					view.getContext().startActivity(browserIntent);
				}
			}
		});

		TextView titleTextView = holder.getTrailerTitleTextView();
		titleTextView.setText(trailer.title);
	}

	@Override
	public int getItemCount() {
		if (trailers == null) {
			return 0;
		}
		return trailers.size();
	}

	class TrailerViewHolder extends RecyclerView.ViewHolder {
		private ImageView playTrailerImageView;
		private TextView trailerTitleTextView;

		TrailerViewHolder(View itemView) {
			super(itemView);

			playTrailerImageView = itemView.findViewById(R.id.iv_play_trailer);
			trailerTitleTextView = itemView.findViewById(R.id.tv_trailer_title);
		}

		ImageView getPlayTrailerImageView() {
			return playTrailerImageView;
		}

		TextView getTrailerTitleTextView() {
			return trailerTitleTextView;
		}
	}
}
